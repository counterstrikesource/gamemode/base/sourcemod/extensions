#!/bin/bash

cd "$(pwd)""/extensions/"
find "$(pwd)" -maxdepth 1 -type d | grep -i "sm-ext" | while read extension; do
	cd $extension
	echo "$(pwd)"
	mkdir "$(pwd)""/build"
	cd "$(pwd)""/build"
	python3 "$(pwd)""/../configure.py"
	python3 "$(pwd)""/../configure.py" --sdks=$1
	python3 "$(pwd)""/../configure.py" --enable-optimize
	python3 "$(pwd)""/../configure.py" --enable-optimize --sdks=sdk2013
	python3 "$(pwd)""/../configure.py" --enable-optimize --sdks=$1
	ambuild
	mkdir -p "$(pwd)""/../../../build/package_extensions/"
	cp -R "$(pwd)"/package/* "$(pwd)""/../../../build/package_extensions/"
done
